<?php

return [
    'app_name' => 'MVC Micro Framework',

    'providers' => [
        Core\Application\Application::class => 'Application'
    ]
];