<?php

spl_autoload_register(function ($className) {
    $path = '..';
    
    if (!str_start_with($className, 'App'))
    {
        $path .= '/vendor';
    }

    preg_match('/(.*?)(\w+Controller)$/', $className, $matches);
    [, $namespace, $class] = $matches;
    $nsTokens = explode('\\', substr($namespace, 0, strlen($namespace) - 1));
    
    foreach ($nsTokens as $token)
    {
        $path .= '/' . strtolower($token);
    }

    $path .= '/' . $class . '.php';
    //die($path);
    require_once($path);
});