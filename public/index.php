<?php

defined('ROOT_DIR') || define('ROOT_DIR', realpath(__DIR__.'/..'));

require_once('../vendor/functions.php');
require_once(ROOT_DIR . '/vendor/autoload.php');

$app = require_once('../vendor/Core/Application.php');
$app->handleRequest();