<?php

namespace Core;

/**
 * @method config
 * @method registerConfig
 */
class Application
{
    private array $configs = [];

    public function __construct($showErrors=false)
    {
        $this->_registerConfig('app.php');

        if ($showErrors) {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }
    }

    public function config($configKey) 
    {
        $value = NULL;
        
        [$configName] = explode('.', $configKey);
        $keys = array_slice(explode('.', $configKey), 1, count(explode('.', $configKey)) - 1);
        
        if (empty($configName) || !array_key_exists($configName, $this->configs))
        {
            # throw new Exception('There is no config with name ' . $config . '!');
            return;
        }
        
        if (count($keys) <= 0)
        {
            # throw new Exception('You have to specify a key of the config!');
            return;
        }

        $value = $this->configs[$configName];
        foreach ($keys as $key)
        {
            $value = $value[$key];
        }

        return $value;
    }

    public function handleRequest()
    {
        $requestUrl = substr($_SERVER['REQUEST_URI'], 1);
        $urlTokens = explode('/', $requestUrl);

        if (empty($urlTokens[0]))
        {
            return;
        }

        $controllerClass = 'App\Controllers\\' . ucfirst($urlTokens[0]) . 'Controller';
        $controller = new $controllerClass;

        die($controllerClass);
    }

    private function _registerConfig(string $configFile)
    {
        preg_match('/^(.*?)[.]php$/', $configFile, $matches);
        $config = require_once(ROOT_DIR . '/config/' . $configFile);

        $this->configs[$matches[1]] = $config;
    }
}

return new \Core\Application(true);