<?php

if (!function_exists('str_start_with'))
{
    function str_start_with(string $str, string $start_with)
    {
        return substr($str, 0, strlen($start_with)) == $start_with;
    }
}